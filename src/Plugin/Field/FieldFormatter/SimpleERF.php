<?php

namespace Drupal\simple_erf\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Simple Entity Reference Formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "simple_erf",
 *   label = @Translation("Simple Entity Reference Formatter"),
 *   description = @Translation("Choose the display formatting for entity references"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SimpleERF extends EntityReferenceFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'show_commas' => 'yes',
      'show_trailing_comma' => 'no',
      'link_to_entity' => 'yes',
      'display_options' => 'inline',
      'show_term_description' => 'no',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $yesNoOptions = [
      'yes' => 'Yes',
      'no' => 'No',
    ];

    $displayOptions = [
      'inline' => 'Display inline',
      'list' => 'Display as list',
    ];

    $element = [];

    $element['show_commas'] = [
      '#title' => $this->t('Separate items by commas'),
      '#description' => t('Places a comma after each item except the last (for display inline only).'),
      '#type' => 'select',
      '#options' => $yesNoOptions,
      '#required' => 'required',
      '#default_value' => $this->getSetting('show_commas') ?: 'yes',
    ];

    $element['show_trailing_comma'] = [
      '#title' => $this->t('Show a trailing comma?'),
      '#description' => t('Places a comma after the last item (for display inline only, useful in multiple field instances).'),
      '#type' => 'select',
      '#options' => $yesNoOptions,
      '#required' => 'required',
      '#default_value' => $this->getSetting('show_trailing_comma') ?: 'no',
    ];

    $element['link_to_entity'] = [
      '#title' => $this->t('Link item to entity?'),
      '#description' => t('Wraps item in a link tag to the entity.'),
      '#type' => 'select',
      '#options' => $yesNoOptions,
      '#required' => 'required',
      '#default_value' => $this->getSetting('link_to_entity') ?: 'yes',
    ];

    $element['display_options'] = [
      '#title' => $this->t('Display options'),
      '#description' => t('Defines how this content is displayed.'),
      '#type' => 'select',
      '#options' => $displayOptions,
      '#required' => 'required',
      '#default_value' => $this->getSetting('display_options') ?: 'inline',
    ];
    $element['show_term_description'] = [
      '#title' => $this->t('Show term description.'),
      '#description' => t('Show the term description (for List display only, for taxonomy terms only).'),
      '#type' => 'select',
      '#options' => $yesNoOptions,
      '#default_value' => $this->getSetting('show_term_description') ?: 'no',
      '#states' => [
        'visible' => [
          'select[name="fields[field_term_reference][settings_edit_form][settings][display_options]"]' => [
            'value' => 'list',
          ],
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];
    $settingsList = [];
    $settings = $this->getSettings();

    $summary[] = $this->t('Separate With Commas?@spacer', ['@spacer' => ' ']) . $settings['show_commas'];
    $summary[] = $this->t('Show Trailing Comma?@spacer', ['@spacer' => ' ']) . $settings['show_trailing_comma'];
    $summary[] = $this->t('Link to Entity?@spacer', ['@spacer' => ' ']) . $settings['link_to_entity'];
    $summary[] = $this->t('Display@spacer', ['@spacer' => ': ']) . $settings['display_options'];
    $summary[] = $this->t('Show Term Description@spacer', ['@spacer' => ': ']) . $settings['show_term_description'];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    // Verify langcode is set.
    if (empty($langcode)) {
      $langcode = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    }

    // Get the field name our formatter is applied to.
    $fieldName = $items->getName();

    // Get our form settings.
    $showCommas = $this->getSetting('show_commas');
    $showTrailingComma = $this->getSetting('show_trailing_comma');
    $linkToEntity = $this->getSetting('link_to_entity');
    $displayOptions = $this->getSetting('display_options');
    $showTermDescription = $this->getSetting('show_term_description');

    // Instantiate our variables.
    $elements = [];
    $index = 0;
    $totalCount = count($items);

    // Loop through our field entities.
    foreach ($this->getEntitiesToView($items, $langcode) as $key => $item) {

      // Iterate index.
      $index++;

      // Instantiate our variables.
      $name = NULL;
      $termDescription = NULL;
      $url = NULL;
      $provider = $item->getEntityType()->getProvider();

      // Check if content reference.
      if ($provider === 'node') {
        $name = $item->label();
        $url = $item->url();
      }

      // Check if taxonomy reference.
      if ($provider === 'taxonomy') {
        $name = $item->getName();
        $termDescription = $item->getDescription();
        $url = $item->url();
      }

      // Check if user reference.
      if ($provider === 'user') {
        $name = $item->getDisplayName();
        $url = Url::fromRoute('entity.user.canonical', ['user' => $item->id()])->toString();
      }

      // Build our element array.
      $elements[$key] = [
        '#theme' => 'simple_erf',
        '#bundle' => $item->bundle(),
        '#provider' => $provider,
        '#name' => $name,
        '#url' => $url,
        '#fieldName' => $fieldName,
        '#showCommas' => $showCommas === 'yes' && $index < $totalCount ? ', ' : '',
        '#trailingComma' => $showTrailingComma === 'yes' && $index == $totalCount ? ', ' : '',
        '#linktoentity' => $linkToEntity,
        '#displayOptions' => $displayOptions,
        '#showTermDescription' => $showTermDescription,
        '#termDescription' => $termDescription,
      ];

    }
    return $elements;
  }

}
